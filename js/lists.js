
const serversName = 'http://192.168.1.211:8008/HandlarlistaService.svc';



//GetShopLists/{userId}
//при загрузки страницы все списки пользоателя загружаются автоматически
window.addEventListener('load', ()=> {
    let userLists;
    getShopLists()
    
    .then( res => {
        userLists = res;
        return Promise.all(userLists);
    })
    .then(res => {
        
        res.forEach(cur => {
            addListBlock(cur.Date, cur.Id);
        })
    })
    .catch(console.log.bind(console));
    
   
});
    
//функция с запросом к серверу на получения списка пользователя
async function getShopLists() {
    
    try {
        const response = await fetch(`${serversName}/GetShopLists/${localStorage.id}`);
        let data = await response.json();
        let lists = data.GetShopListsResult;  
        return lists;
        
    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    }
};   

/******************/


//CreateShopList/{userId}
//функция с запросом к серверу на создание нового списка пользователя
async function createShopList() {
    
    try {
        const response = await fetch(`${serversName}/CreateShopList/${localStorage.id}`);
        let data = await response.json();
        let id = data.CreateShopListResult;
        return id;
        
    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    }
}; 

document.getElementById('add-list__btn').addEventListener('click', ()=> {
    let id;
    createShopList()
    .then( res => {
        id = res;
        return id;

    })
    .then(id => {
        addListBlock(getData(), id);
        
    })
    .catch(console.log.bind(console));
    
});

/**************************/



function getData() { //создание текущей даты
    var now, months, data;
    now = new Date();
    data = `${now.getDate()}.${now.getMonth()+1}.${now.getFullYear()}`;
    return data;
};

const addListBlock = (data, id) => {
    
    const markup = `
        
        <div class="list-block" id=${id}>
            <div class="list-block__header">
                <div class="add-item__block">
                    <button class="btn add-item__btn">➕</button>
                </div>
                <h1 class="list-block__heading" id="list-block__heading-${id}"> Список ${id} от ${data}</h1>
                <button class="btn delete__btn" id="delete-btn-${id}">✖</button>


            </div>
            <div class="create-section"></div>
            <div class="items-section"></div>
        </div>
    `;
    document.querySelector('.main').insertAdjacentHTML('afterBegin', markup);
  
};


/*******************/

//удалениe списка пользователя 
//DeletShopList/{userId}/{listId}

const deleteList = (event) => {
    let id = event.target.id;
    if(id && event.target.className === 'btn delete__btn') {
        let domList = document.getElementById(id).parentNode.parentNode;
        domList.remove();//удаление списка из UI
        id = domList.id;
        DeletShopList(id);//удаление списка на сервере
    } 
};

document.querySelector('.main').addEventListener('click', deleteList);

async function DeletShopList(listId) {
    try {
        const response = await fetch(`${serversName}/DeletShopList/${localStorage.id}/${listId}`);
        let data = await response.json();

    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    } 
}; 

/*********************/
//Запрос элементов списка
//GetShopListItems/{userId}/{listId}


async function GetShopListItems(idList) {
    
    try {
        const response = await fetch(`${serversName}/GetShopListItems/${localStorage.id}/${idList}`);
        let data = await response.json();
        let arrItems = data.GetShopListItemsResult;
        return arrItems;

    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    } 
}; 
let openedList = {};


document.querySelector('.main').addEventListener('click', (event)=> {
    let classOfHeading = event.target.className;
    
    if(classOfHeading === 'list-block__heading') {
        
        let idList = document.getElementById(event.target.id).parentNode.parentNode.id; //Id списка покупок

        
        if(openedList[idList] === false || !(idList in openedList)) { //false или отсутствует - условия, означающие, что список закрыт
            //add items
            openedList[idList] = true; //переключаем на true - условие, означающее, что список открыт
            
            let arrItems;
            GetShopListItems(idList) //запрашиваем на сервере элементы списка по id
            
            .then( res => {
                arrItems = res;
                for (const cur of arrItems) {
                        addItem(cur.Id, cur.Ishandle, cur.Name, idList); //отображаем в UI элементы списка
                    }
                
            })
        }
        else { //вариант, когда список с элементами уже открыт, щелчок по открытому списку сворачивает его и удаляет элементы
            
            
            document.getElementById(idList).getElementsByClassName('items-section')[0].innerHTML = ``; //remove items
          
            
            openedList[idList] = false; //переключаем на false - условие, означающее, что список закрыт
            
        } 
    
    }
    
    
});

/*********************/
// Функция для отображения элементов в списке


const addItem = (Id, IsHandled, Name, idList) => {
    
    const markup = `
        
        <div class="list-block__item" id="list-${idList}__item-${Id}">
            <span class="list-block__id">${Id}</span>
            <span class="list-block__name">${Name}</span>
            <input type="checkbox" class="list-block__checkbox">
            <button class="delete-item__btn">✖</button>
        </div>
                
       
    `;
/*    <span class="list-block__id">${Id}</span>  разметка для отображения порядкового ID элемента */  
    
   document.getElementById(idList).getElementsByClassName('items-section')[0].insertAdjacentHTML('beforeEnd', markup);

  
};
//                     
//http://192.168.1.211:8008/HandlarlistaService.svc/CreateShopListItem/00671eb3-6bb9-4c99-9a71-4fb53e632ebf/0/хлеб

/*****************/
//Добавление елемента в список
document.querySelector('.main').addEventListener('click', (event)=> {
    let classOfElement = event.target.className;
    
    if(classOfElement === 'btn add-item__btn') {
        
        let idList = findListID(event.target); //находим id списка на котором произошло событие
        let idInput = `input-item-${idList}`; // присваеваем уникальный id полю для ввода
        
        const markup = `<input type="text" class="input-item" id="${idInput}">`; //генерируем разметку
        
        let listChildrenNode = document.getElementById(idList).childNodes; //запрашиваем детей у списка с нашим id
        let listChildrenArr = Array.from(listChildrenNode); //преобразуем полученный NodeList в массив
        let createSection = findSection(listChildrenArr,'create-section'); //находим секцию, в которую будем вставлять разметку
        
        if (!createSection.childNodes.length) { //делаем проверку на наличие уже вставленной разметки (поля для ввода)
            createSection.insertAdjacentHTML('afterBegin', markup); //вставляем разметку (поле для ввода значения )

            GetShopListItems(idList) //запрашиваем на сервере элементы списка по id

            .then( res => { //получаем ответ от сервера и отображаем в UI элементы списка
                openedList[idList] = true; //переключаем на true - условие, означающее, что список открыт
                arrItems = res;
                for (const cur of arrItems) {
                        addItem(cur.Id, cur.Ishandle, cur.Name, idList); //отображаем в UI элементы списка
                    }

            })
            
            const inpitElement = document.getElementById(idInput);
            
            //document.querySelector('.main').addEventListener('keypress', (event)=> {
            inpitElement.addEventListener('keypress', (event)=> {
               console.log(`nbbbn`);
                if(event.target.id.includes(`input-item-${idList}`) && (event.keyCode === 13 || event.which === 13)) {
                    event.preventDefault();     
                    let valueInput;
                    valueInput = document.getElementById(idInput).value; //сохраняем введеное пользователем значение
                    
                    if(valueInput) {
                        console.log(valueInput)
                        CreateShopListItem(idList, valueInput) // отправление item на сервер и сохраняем его ID в переменную

                        .then( res => {
                            addItem(res, 0, valueInput, idList); // добавляем в UI данный элемент
                            document.getElementById(idInput).value   = ''; //очищаем поле ввода
                        })
                    }
                    
                    
                }
            });
        } 
        
        
        
    }
});

const findSection = (listChildrenArr, sectionName) => {
    
    for(var i = 0; i<listChildrenArr.length; i++){
        if(listChildrenArr[i].className === sectionName)
            return listChildrenArr[i];
    }
    
    return -1;
//    console.log('cant find sectionName');
//    alert('Something went wrong :(');
};

const findListID = (curNode) => {
    
    while(curNode !== null){
        if(curNode.className === "list-block") {
            return curNode.id;
        }
        else{ 
            curNode = curNode.parentNode;
        }
    }
    console.log('cant find list-block');
    alert('Something went wrong :(');
};

async function CreateShopListItem(idList, itemName) {
    
    try {
        const response = await fetch(`${serversName}/CreateShopListItem/${localStorage.id}/${idList}/${itemName}`);
        let data = await response.json();
        
        let idItem = data.CreateShopListItemResult;
        return idItem;

    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    } 
}; 


/*удаление элемента из списка*/
async function DeleteShopListItem(listId, itemId) {
    let response;
    try {
        response = await fetch(`${serversName}/DeleteShopListItem/${localStorage.id}/${listId}/${itemId}`);
        
        let data = await response.json();
        
//        let idItem = data.CreateShopListItemResult;
        return data;
        console.log(data);

    } catch (error) {
        console.log(error);
        console.log(response);
        alert('Something went wrong :(');
    } 
}; 

document.querySelector('.main').addEventListener('click', (event)=> {
    let classOfElement = event.target.className; //delete-item__btn
    
    
    if(classOfElement === `delete-item__btn`) {
        let idItemDOM = event.target.parentNode.id; //list-0__item-0 строка
        let idList = findListID(event.target); //находим id списка на котором произошло событие
        let idlistServer = idItemDOM.slice(13); 
        document.getElementById(idItemDOM).remove(); 
        DeleteShopListItem(idList, idlistServer);
    }
    
    
    
    
});

/* DeleteShopListItem/{userId}/{listId}/{itemId}
//не работающий вариант
document.querySelector('.main').addEventListener('click', (event)=> {
    let classOfElement = event.target.className;
    
    
    if(classOfElement === `delete-item__btn`) {
        let idItemDOM = event.target.parentNode.id; //list-block__item-0 строка
        let itItemServer = parseInt(idItemDOM.slice(17));
        console.log(` id item ${itItemServer}`);
        let idList = findListID(event.target);
        console.log(`id list - ${idList}`);
        DeleteShopListItem(idList, itItemServer);
        document.getElementById(idItemDOM).remove();
        

    }
    
});
${localStorage.id}
fetch(`${serversName}/DeletShopList/${localStorage.id}/${listId}`);

const serversName = 'http://192.168.1.211:8008/HandlarlistaService.svc';



//GetShopLists/{userId}
//при загрузки страницы все списки пользоателя загружаются автоматически
window.addEventListener('load', ()=> {
    let userLists;
    getShopLists()
    
    .then( res => {
        userLists = res;
        return Promise.all(userLists);
    })
    .then(res => {
        
        res.forEach(cur => {
            addListBlock(cur.Date, cur.Id);
        })
    })
    .catch(console.log.bind(console));
    
   
});
    
//функция с запросом к серверу на получения списка пользователя
async function getShopLists() {
    
    try {
        const response = await fetch(`${serversName}/GetShopLists/${localStorage.id}`);
        let data = await response.json();
        let lists = data.GetShopListsResult;  
        return lists;
        
    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    }
};   

/******************/

/*
//CreateShopList/{userId}
//функция с запросом к серверу на создание нового списка пользователя
async function createShopList() {
    
    try {
        const response = await fetch(`${serversName}/CreateShopList/${localStorage.id}`);
        let data = await response.json();
        id = data.CreateShopListResult;
        return id;
        
    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    }
}; 

document.getElementById('add-list__btn').addEventListener('click', ()=> {
    let id;
    createShopList()
    .then( res => {
        id = res;
        return id;

    })
    .then(id => {
        addListBlock(getData(), id);
        
    })
    .catch(console.log.bind(console));
    
});

/**************************/

/*

function getData() { //создание текущей даты
    var now, months, data;
    now = new Date();
    data = `${now.getDate()}.${now.getMonth()+1}.${now.getFullYear()}`;
    return data;
};

const addListBlock = (data, id) => {
    
    const markup = `
        
        <div class="list-block">
            <div class="list-block__header">
                <div class="add-item__block">
                    <button class="btn add-item__btn">➕</button>
                </div>
                <h1 class="list-block__heading" id=${id} >Список ${id} от ${data}</h1>
                <button class="btn delete__btn" id="delete-btn-${id}">✖</button>


            </div>
            <div class="create-section"></div>
            <div class="items-section"></div>
        </div>
    `;
    document.querySelector('.main').insertAdjacentHTML('afterBegin', markup);
  
};


/*******************/
/*
//удалениe списка пользователя 
//DeletShopList/{userId}/{listId}

const deleteList = (event) => {
    let id = event.target.id;
    if(id && event.target.className === 'btn delete__btn') {
        let domList = document.getElementById(id).parentNode.parentNode;
        domList.remove();
        id = domList.childNodes[1].children[1].id;
        DeletShopList(id);
    } 
};

document.querySelector('.main').addEventListener('click', deleteList);

async function DeletShopList(listId) {
    

        try {
            const response = await fetch(`${serversName}/DeletShopList/${localStorage.id}/${listId}`);
            let data = await response.json();
            console.log(data);

        } catch (error) {
            console.log(error);
            alert('Something went wrong :(');
        } 
    
}; 

/*********************/
//Запрос элементов списка
//GetShopListItems/{userId}/{listId}

/*
async function GetShopListItems(idList) {
    
    try {
        const response = await fetch(`${serversName}/GetShopListItems/${localStorage.id}/${idList}`);
        let data = await response.json();
        let arrItems = data.GetShopListItemsResult;
        return arrItems;

    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
    } 
}; 
let openedList = {};


document.querySelector('.main').addEventListener('click', (event)=> {
    if(event.target.className === 'list-block__heading') {
        
        idList = event.target.id; //Id списка покупок
      
        if(openedList[idList] === false || !(idList in openedList)) {
            //add items
            openedList[idList] = true;
            
            let arrItems;
            GetShopListItems(idList)
            
            .then( res => {
                arrItems = res;
                for (const cur of arrItems) {
                        addItem(cur.Id, cur.Ishandle, cur.Name);
                    }
                
            })
        }
        else {
            //remove items
            
            document.getElementById(idList).parentNode.parentNode.getElementsByClassName('items-section')[0].innerHTML = ``;
          
            
            openedList[idList] = false;
            
        } 
    
    }
    
    
});

const addItem = (Id, IsHandled, Name) => {
    
    const markup = `
        
        <div class="list-block__item">
            <span>${Id}</span>
            <span>${Name}</span>
            <input type="checkbox">
        </div>
                
       
    `;
  
    
   document.getElementById(idList).parentNode.parentNode.getElementsByClassName('items-section')[0].insertAdjacentHTML('beforeEnd', markup);

  
};
//                     
//http://192.168.1.211:8008/HandlarlistaService.svc/CreateShopListItem/00671eb3-6bb9-4c99-9a71-4fb53e632ebf/0/хлеб
*/




















































