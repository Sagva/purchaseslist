const serversName = 'http://192.168.1.211:8008/HandlarlistaService.svc';


//let accounts = {
//    login: localStorage.getItem('login'),
//    pass: localStorage.getItem('password')
//};




document.getElementById('submit_id').addEventListener('click', async ()  => { //обработчик событий на кнопку отправить
    
    var login = document.getElementById('login_id').value;
    var pass = document.getElementById('password_id').value;
    
    try {
        
        const response = await fetch(`${serversName}/Login/${login}/${pass}`);
        let data = await response.json();
        let id = data.LoginResult;
            
        localStorage.setItem('id', id);
        
        document.location.href = "lists.html";
        
    } catch (error) {
        console.log(error);
        alert('Something went wrong :(');
        document.location.href = "login.html";
    }
    
    
});
